# Kleine Hilfe zur Ticketerstellung:

- Bitte durchsuche zuerst die bestehenden Tickets ob es bereits ein passendes Ticket gibt.
- Falls Du eine Übersetzung zu einem freien Dienst hinzufügen willst, wähle bitte oben die Vorlage "Freien Dienst hinzufügen".
- Ansonsten lösche diesen Hilfstext und beschreibe Dein Anliegen.