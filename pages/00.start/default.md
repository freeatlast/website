---
title: Willkommen
body_classes: title-center title-h1h2
---
# Switching Social
## Wechsle zu Freier Software

An dieser Stelle entsteht eine deutschsprachige Version der populären englischen Webseite [switching.social](https://switching.social).

**SwitchingSocial** möchte Dich beim Wechsel zu freier und offener Software unterstützen. Unser Ziel ist, dass Du proprietären, unfreien Programmen und Plattformen einfach den Rücken zukehren kannst. So möchten wir Deine Privatsphäre, Deine Meinungsfreiheit und Dein Recht auf Selbstbestimmung stärken.

**Erste Artikel** zu [Maps](/ersetze/google-maps), [Twitter](/ersetze/twitter) und [YouTube](/ersetze/youtube) sind bereits begonnen. Für ein wirklich umfassendes Informations-Angebot wird aber noch etwas Zeit ins Land gehen :-)

Bis dahin erfährst Du anbei mehr über [die Ursprünge von SwitchingSocial](https://switching.social/about-this-site/). Wenn Du Anregungen zum Thema hast kannst Du sie uns [über dieses Kontaktformuar](https://www.brickup.de/kontakt) zukommen lassen. 