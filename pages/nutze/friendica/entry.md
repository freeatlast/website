---
title: Friendica

taxonomy:
    replaces: twitter
    tag: fediverse
---
Friendica ist ein soziales Netzwerk ähnlich zu Facebook. Es bietet Funktionen wie Events/Kalender, Fotoalben und Gruppen.

Als dezentrales Netzwerk besteht es aus vielen kleinen Seiten (Knoten), die untereinander verbunden sind. Über das [Fediverse](../fediverse) lässt sich zudem mit Nutzern von Mastodon und Co. interagieren.

! **Registrierung:** [Nerdica.net](https://nerdica.net/) | [Libranet.de](https://libranet.de/) | [mehr deutschsprachige](https://the-federation.info/friendica#nodes-table)
! **Android-Apps:** [Fedilab](https://fedilab.app/)

===