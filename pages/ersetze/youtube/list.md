---
title: Tschüss Youtube
content:
    items: 
        - '@taxonomy.replaces': youtube
hero_classes: hero-tiny title-h1h2
---
# Tschüss Youtube
## Freie Videoplattformen